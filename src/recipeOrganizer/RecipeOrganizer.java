package recipeOrganizer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;




public class RecipeOrganizer {
	
	private EntityManagerFactory emfactory;
	private EntityManager entitymanager;
	private Recipe newRecipe;
	
	
	public RecipeOrganizer() {
		this.newRecipe = new Recipe();
		
	}
	
	public void addRecipe(Recipe recipe) {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("RecipeBook");
	      
	      EntityManager entitymanager = emfactory.createEntityManager( );
	      entitymanager.getTransaction( ).begin( );
	      
	      
	      setRecipe(recipe);
	      
	      entitymanager.persist(getRecipe());
	      
	      entitymanager.getTransaction().commit( );
	      

	      entitymanager.close( );
	      emfactory.close();
	}
	
	public Recipe findRecipe(int eid) throws IllegalArgumentException {
		
	      EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("RecipeBook");
	      
	      EntityManager entitymanager = emfactory.createEntityManager();
	      entitymanager.getTransaction( ).begin( );
	      
	      Recipe recipe = entitymanager.find(Recipe.class, eid);
	      
	      return recipe;
		
		
	}

	public Recipe getRecipe() {
		return newRecipe;
	}

	public void setRecipe(Recipe recipe) {
		this.newRecipe = recipe;
	}

	
	
	
}
