package recipeOrganizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;


/**
 * 
 * @author Andreas Pichler
 * @version 0.1 11.10.18
 * 
 *
 */
@Entity
@Table
public class Recipe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	
	private String recipeName;
	private String preparation;
	private String category;
	private String portions;
	
	
	public Recipe(int id, String recipeName, String preparation, String kategory, String portions) {
		super();
		this.id = id;
		this.recipeName = recipeName;
		this.preparation = preparation;
		this.category = kategory;
		this.portions = portions;
	}
	
	public Recipe() {
		super();
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getRecipeName() {
		return recipeName;
	}

	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}

	public String getPreparation() {
		return preparation;
	}

	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String kategory) {
		this.category = kategory;
	}

	public String getPortions() {
		return portions;
	}

	public void setPortions(String portions) {
		this.portions = portions;
	}
	
	@Override
	public String toString() {
		return "Id: " + getid() + " RecipeName: " + getRecipeName() + " Preparation: " + getPreparation() + " Kategory: " + getCategory() + " Portions: " + getPortions();
	}

}
