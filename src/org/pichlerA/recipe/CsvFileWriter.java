package org.pichlerA.recipe;

import recipeOrganizer.Recipe;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CsvFileWriter {
	
	private static final String COMMA_DELIMITER = ";";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final String[] WEEKDAYS = {"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So" } ;
	
	//CSV file header
	private static final String FILE_HEADER = "Wochen Rezepte: ";
	
	public void writeCsvFile(Recipe[] recipeList) {
		
		
		
		
		try {
			
			File file = new File("D:\\Programms18\\RecipeBook\\src\\weekrecipes.csv");
			
			if(!(file.exists())) {
				file.createNewFile();
			}
			
			FileWriter fileWriter = new FileWriter(file);
			
			
			
			fileWriter.append(FILE_HEADER.toString());
			
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			for(int i = 0; i < WEEKDAYS.length; i++) {
				fileWriter.append(WEEKDAYS[i]);
				fileWriter.append(COMMA_DELIMITER);
			}
			
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			for(int i = 0; i < recipeList.length; i++) {
				fileWriter.append(recipeList[i].getRecipeName());
				fileWriter.append(COMMA_DELIMITER);
			}
			
			
			
			System.out.println("CSV file created sucessfully in " + file.getAbsolutePath());
			
			
			fileWriter.close();
			
			
		}catch(IOException e){
			
			System.out.println("Somthing went wrong! ");
			e.printStackTrace();
			
		}
		
	}

}
