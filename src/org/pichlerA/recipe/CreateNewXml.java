package org.pichlerA.recipe;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import recipeOrganizer.Recipe;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class CreateNewXml {
	
	Recipe re;
	String rezeptXml;
	
	public CreateNewXml(Recipe re) {
		this.re = re;
		this.rezeptXml = "";
		
	}
		
	public String createXml() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			
			
			Document doc = builder.newDocument();
			//doc.setXmlStandalone(true);
			
			
			Element rootElement = doc.createElement("rezpet");
			doc.appendChild(rootElement);
			
			Element recipeId = doc.createElement("id");
			recipeId.appendChild(doc.createTextNode(Integer.toString(re.getid())));
			rootElement.appendChild(recipeId);
			
			
			Element recipeElement = doc.createElement("rezeptname");
			recipeElement.appendChild(doc.createTextNode(re.getRecipeName()));
			rootElement.appendChild(recipeElement);
			
			Element cooking = doc.createElement("zubereitung");
			cooking.appendChild(doc.createTextNode(re.getPreparation()));
			rootElement.appendChild(cooking);
			
			Element kategory = doc.createElement("kategorie");
			kategory.appendChild(doc.createTextNode(re.getCategory()));
			rootElement.appendChild(kategory);
			
			Element portion = doc.createElement("anzahlPortion");
			portion.appendChild(doc.createTextNode(re.getPortions()));
			rootElement.appendChild(portion);
			
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			StringWriter stringWriter = new StringWriter();
			
			transformer.transform(new DOMSource(doc), new StreamResult(stringWriter));
			
			this.rezeptXml = stringWriter.getBuffer().toString();
			
			
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource dom = new DOMSource(doc);
			File file = new File("D:\\Programms18\\RecipeBook\\src\\recipe.xml");
			StreamResult result = new StreamResult(file);
			transformer.transform(dom, result);
			
			
			//StreamResult re = new StreamResult(System.out);
			
			
			System.out.println("File recipe.xml was created at " + file.getAbsolutePath());
			
		}
		
		catch(TransformerException e) {
			e.printStackTrace();
		}
		catch(ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		
		return rezeptXml;
		
	}

}
