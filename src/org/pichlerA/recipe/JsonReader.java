package org.pichlerA.recipe;

import recipeOrganizer.Recipe;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class JsonReader {
	
	private Recipe[] recipes;
	
	public JsonReader() {
		this.recipes = null;
	}
	
	public Recipe[] readJson(String searchRecipe){
		
		Recipe[] recipeArray = null;
		
		try {

			URL url = new URL("http://localhost/php/REST/RecipeSearch.php?recipe=" + searchRecipe);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			
			
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			
		

			String output;
			System.out.println("Output from Server .... \n");
			
			while ((output = br.readLine()) != null) {
				
				try {	
					Gson gson = new GsonBuilder().create();
					recipeArray = gson.fromJson(output, Recipe[].class);		            
				}
				catch(JsonIOException e) {
					e.printStackTrace();
						
				}
				catch(JsonSyntaxException e) {
					e.printStackTrace();
				}
				
			}
			
			conn.disconnect();
			

		  } catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		
		setRecipeList(recipeArray);
		
		return getRecipeList();
		
	}

	public Recipe[] getRecipeList() {
		return recipes;
	}

	public void setRecipeList(Recipe[] recipes) {
		this.recipes = recipes;
	}
	

}
