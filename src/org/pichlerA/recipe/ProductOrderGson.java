package org.pichlerA.recipe;


public class ProductOrderGson {
	
		
		private int qty;
		private String name; 

		@Override
	    public String toString() {
	        return qty + " --- " + name;
	    }

		public int getQty() {
			return qty;
		}

		public void setQty(int qty) {
			this.qty = qty;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		

}
