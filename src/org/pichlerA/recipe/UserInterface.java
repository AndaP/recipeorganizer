package org.pichlerA.recipe;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import recipeOrganizer.Recipe;

import recipeOrganizer.RecipeOrganizer;
/**
 * 
 * @author Andreas Pichler
 *
 */
public class UserInterface {
	
	private Scanner scan;
	private String xmlOut;
	private String recipe;
	private Recipe re;
	private Recipe[] recipeList;
	private boolean checkonce;
	private boolean checktwice;
	private String outputRecipe;
	private RecipeOrganizer organizer;
	
	/**
	 * 
	 */
	public UserInterface() {
		this.scan = new Scanner(System.in);
		this.xmlOut = "";
		this.recipe = "";
		this.re = null;
		this.recipeList = null;
		this.checkonce = false;
		this.checktwice = false;
		this.outputRecipe = "";
		this.organizer = new RecipeOrganizer();
	}
	
	
	/**
	 * Startet das UserInterface
	 */
	public void getStart() {
		System.out.println("*******************************************************************************************");
		System.out.println("*                Willkommen bei 'Kochen mit Liebe' Ihr eigenes Kochbuch                   *");
		System.out.println("*******************************************************************************************");
		
		int choice = 0;
		boolean check = false;
		do {
			
			System.out.println("\n\nBitte w�hlen Sie ihre Obtion: \n"
					+ "1. Rezept suchen aus Online Datenbank und Speichern\n"
					+ "2. Wochen Rezepte generieren\n"
					+ "3. Neues Rezept erstellen und speichern \n"
					+ "4. Rezept suchen aus tempor�ren Datenbank\n"
					+ "5. Ende");
			
			try {
				choice = Integer.valueOf(scan.nextLine());
				
				
			}catch(NumberFormatException e) {
				System.out.println("Bitte keine W�rter!");
			}
			
			switch(choice){
			
			case 1: searchRecipe();
					break;
					
			case 2: generateWeekRecipes();
					break;
				
			case 3: addNewRecipe();
					break;
					
			case 4: findRecipe();
					break;
					
			case 5: check = true;
					System.out.println("Tschau...");
					break;
				
			default: System.out.println("Bitte richtige Wahl treffen! ");
			
			}
			
			
		}while(!check);
			
	}
	
	private void findRecipe() {
		
		do {
			int id = 0;
			System.out.println("Tippen Sie eine ID ein um das Rezept zu finden");
			
			try{
				id = Integer.valueOf(scan.nextLine());
				Recipe recipe = organizer.findRecipe(id);
				System.out.println("\nGefundenes Rezept: \n" + recipe.toString());
				break;
				
			}catch(NumberFormatException e) {
				System.out.println("Bitte keine Woerter ++");
			}catch(IllegalArgumentException e) {
				System.out.println("Kein Rezept mit der id " + id + " gefunden!");
			};
			
			
		}while(true);
		
		
		
		
		
	}
	
	private void generateWeekRecipes() {
		
			boolean check = false;
			JsonReader reader = new JsonReader();
			recipeList = reader.readJson(recipe);
			
			
			do {
				
				Recipe[] recipeWeekList = getWeekList(recipeList);	
				
				do {
					
					System.out.println("M�chten Sie diese Wochenrezepte in einer CSV speichern oder neu genieren (y/n)");	
					
					String answer = scan.nextLine().toLowerCase();
					
					if(answer.equals("y")) {
						
						CsvFileWriter csvFile = new CsvFileWriter();
						csvFile.writeCsvFile(recipeWeekList);
						check = true;
						break;
						
					}
					else if(answer.equals("n")) {
						
						System.out.println("Rezepte werden neu generiert...\n\n");
						break;
					}
					else {
						System.out.println("Bitte geben Sie eine gueltige Eingabe ein");
					}
					
					
					
				}while(true);
				
				
				
				
				
			}while(!check);
			
			
			
			
			
		
		
		
	}
	
	private Recipe[] getWeekList(Recipe[] recipeList) {
		

		
		Recipe[] newList = new Recipe[7];
		
		
		ArrayList<Integer> rndList = getRndNumber();
		
		for(int i = 0; i < newList.length; i++) {
			
			newList[i] = recipeList[rndList.get(i)];
			
		}
		
		for(int i = 0; i < newList.length; i++) {
			
			System.out.println(newList[i].getRecipeName());
		}
		
		return newList;
	}
	
	private ArrayList<Integer> getRndNumber(){
		
		Random rnd = new Random();
		
		ArrayList<Integer> rndList = new ArrayList<>();
		
		
		do{
			
			int rndNumber = rnd.nextInt(recipeList.length-1);
			
			if(rndList.isEmpty()) {
				
				rndList.add(rndNumber);
			}
			else if(!(rndList.contains(rndNumber))) {
				
				
				rndList.add(rndNumber);
			}
			
		}while(rndList.size() !=7);   //7 fuer die Wochentage
		
		return rndList;
	}
	
	

	
	private void addNewRecipe() {
		boolean check = false;
		System.out.println("Rezept neu Eingabe: ");
		
		do {
			
			
			try {
				System.out.println("Rezept Name: ");
				String recipeName = scan.nextLine();
				System.out.println("Rezept Zubereitung mit Zutaten: ");
				String preparation = scan.nextLine();
				System.out.println("Anzahl Portionen: ");
				String portions = scan.nextLine();
				System.out.println("Kategorie: ");
				String category = scan.nextLine();
				Recipe basicRecipe = new Recipe();
				
				basicRecipe.setPortions(portions);
				basicRecipe.setCategory(category);
				basicRecipe.setPreparation(preparation);
				basicRecipe.setRecipeName(recipeName);
				
				
				System.out.println("Wollen Sie das Rezept wirklich speichern?? y/n");
				
				do {
					
					String answer = scan.nextLine().toLowerCase();
					
					System.out.println(answer);
					
					if(answer.equals("y")) {
						organizer.addRecipe(basicRecipe);
						System.out.println("Rezept: " + basicRecipe.getRecipeName() + " wurde gespeichert");
						check = true;
						break;
					}
					else if(answer.equals("n")) {
						System.out.println("Neue Eingabe.....\n\n\n");
						break;
					}
					else {
						System.out.println("Bitte treffen Sie eine richtige Wahl!");
					}
					
				}while(true);
				
				
				
				
				
			}catch(NumberFormatException e) {
				e.printStackTrace();
			}
			
			
			
			
			
		}while(!check);
		
	}
	
	/**
	 * Sucht ein Rezept aus einer Datebank 
	 */
	private void searchRecipe() {
		do {
			
				
				System.out.println("Bitte geben Sie das Rezept ein was sie Suchen Wollen...");
				
				recipe = scan.nextLine();
				
				try {
					
					JsonReader reader = new JsonReader();
					recipeList = reader.readJson(recipe);
					//re = reader.getRecipe();	
					
					if(recipeList !=null) {
						if(recipeList.length == 1) {
							this.outputRecipe = recipeList[0].toString();
							System.out.println("Gefundenes Rezept: " + recipeList[0].getRecipeName() + "\n");
							this.re = recipeList[0];
							checkonce = true;
						}
						else {
							
							do {
								System.out.println("Mehrere Gefunden bitte w�hlen Sie eines aus, dass Sie speichern wollen\n");
							
								for(int i = 0; i < recipeList.length; i++) {
									System.out.println((i+1) + ". " + recipeList[i].getRecipeName());
								}
							
								try {
									int choice = Integer.valueOf(scan.nextLine());
									
									this.outputRecipe = recipeList[choice-1].toString();
									this.re = recipeList[choice-1];
									checkonce = true;
									break;
								
								
								}catch(NumberFormatException e) {
									System.out.println("Bitte keine Woerter!");
								
								}catch(IndexOutOfBoundsException e) {
									System.out.println("Bitte richte Wahl treffen...");
								}
							}while(true);
							
						}
						
					}
					else {
						System.out.println("Nichts gefunden..");
					}
					
					
				}catch(Exception e) {
					
					System.out.println("Es ist was schief gelaufen..!");
					e.printStackTrace();
				}	
			
		}while(!checkonce);
		saveRecipe();
		
	}
	
	
	/**
	 * Speichert Rezept als XML oder in temporaere Datenbank 	
	 */
	private void saveRecipe() {	
		do {
			
			int choice = 0;
			String choiceString = "";
			
			System.out.println("Was ist Ihr naechster Schritt \n1. In Temporaere Datenbank speichern \n2. Erstellen einer XML \n3. Rezept in Klartext ausgeben ");
			
			try {
				choice = Integer.valueOf(scan.nextLine());
			}catch(NumberFormatException e) {
				System.out.println("Bitte keine Woerter!");
			}
			
	
			
			if(choice == 1) {
				choiceString = "save";
			}
			else if(choice == 2) {
				choiceString = "xml";
			}
			else if(choice == 3) {
				choiceString = "klartext";
			}
			
			
			switch (choiceString) {
			
			case "save": 
				
				
				
				organizer.addRecipe(re);
				System.out.println("Rezept wude in Datenbank erfolgreich gepeichert");
				checktwice = true;
				break;
				
			case "xml":
				CreateNewXml createXml = new CreateNewXml(re);
				xmlOut = createXml.createXml();
				
				System.out.println("Wollen Sie die XML gleich ausgeben y/n ?");
				
				String answer = scan.nextLine();
				
				if(answer.equals("y")) {
					System.out.println(xmlOut);
				}
				else {System.out.println("OK");}
				checktwice = true;
				break;
				
			case "klartext":
				
				System.out.println(outputRecipe);
				
				checktwice = true;
				break;
				
			default: 
				
				System.out.println("Anweisung nicht korrekt!");
				break;
			}
				
		}while(!checktwice);
		
			
	}
	
}
